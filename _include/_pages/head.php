<meta charset="utf-8"/>
<title>Halaman Utama | Dashboard</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="_include/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="_include/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="_include/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="_include/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="_include/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="_include/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
<link href="_include/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
<link href="_include/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="_include/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->

<!-- BEGIN THEME STYLES -->
<link href="_include/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="_include/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="_include/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="_include/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="_include/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="_include/plugins/respond.min.js"></script>
<script src="_include/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="_include/plugins/jquery.min.js" type="text/javascript"></script>
<script src="_include/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="_include/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="_include/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="_include/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="_include/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="_include/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="_include/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="_include/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="_include/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="_include/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="_include/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="_include/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="_include/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="_include/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="_include/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="_include/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
<script src="_include/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="_include/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="_include/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="_include/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="_include/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="_include/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
<script src="_include/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="_include/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="_include/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="_include/scripts/metronic.js" type="text/javascript"></script>
<script src="_include/layout/scripts/layout.js" type="text/javascript"></script>
<script src="_include/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="_include/layout/scripts/demo.js" type="text/javascript"></script>
<script src="_include/pages/scripts/index.js" type="text/javascript"></script>
<script src="_include/pages/scripts/tasks.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
Demo.init(); // init demo features
   Index.init();   
   Index.initDashboardDaterange();
   Index.initJQVMAP(); // init index page's custom scripts
   Index.initCalendar(); // init index page's custom scripts
   Index.initCharts(); // init index page's custom scripts
   Index.initChat();
   Index.initMiniCharts();
   Tasks.initDashboardWidget();
});
</script>


<link rel="shortcut icon" href="favicon.ico"/>
