<?php

function db_connect($driver, $host, $user, $pass, $db, $port) {
    switch ($driver) {
        default;
        case "mssql":
            $connection = array("Database" => $db, "UID" => $user, "PWD" => $pass);
            $cn = sqlsrv_connect($host, $connection)
                    or
                    die("Cannot connect to Mic. SQL Server.<br/>" . sqlsrv_errors());
            if (!defined("_driver_"))
                define("_driver_", "mssql");
            break;

        case "mysql":
            $cn = mysql_connect($host, $user, $pass) or
                    die("Cannot connect to MySQL Server.<br/>" . mysql_error());

            mysql_select_db($db) or die("Cannot use database '$db'.");
            if (!defined("_driver_"))
                define("_driver_", "mysql");
            break;

        case "mysqli":
            $cn = mysqli_connect($host, $user, $pass, $db);

            if (!$cn) {
                die("Cannot connect to MySQLi Server.<br/>" . mysqli_error($cn));
            }
            if (mysqli_connect_errno()) {
                die("Connect failed: " . mysqli_connect_errno() . " : " . mysqli_connect_error());
            }

            if (!defined("_driver_"))
                define("_driver_", "mysqli");
            break;

        case "postgresql";
            $cn = pg_connect("host=$host user=$user password=$pass dbname=$db port=$port") or
                    die("Cannot connect to PostgreSQL Server.<br/>" . pg_last_error());

            if (!defined("_driver_"))
                define("_driver_", "postgresql");
            break;
    }

    return $cn;
}

function run_query($q) {
    global $cn, $limit;

    switch (_driver_) {
        default:
        case "mssql":
            $params     = array();
            $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
            $rs         = sqlsrv_query($cn, $q, $params, $cursorType) or die(sqlsrv_errors());
            break;

        case "mysql":
            $rs = mysql_query($q, $cn) or die(mysql_error());
            break;

        case "postgresql":
            $rs = pg_query($q) or die("Table not found..<br/> " . pg_last_error());
            break;
    }


    return $rs;
}

function ms_fetch($rs, $type = "assoc") {
    switch (_driver_) {
        default:
        case "mssql":
            return sqlsrv_fetch_array($rs, SQLSRV_FETCH_ASSOC);
            break;

        case "mysql":
            $drv = "mysql_fetch_{$type}";
            return $drv($rs);
            break;

        case "postgresql":
            $drv = "pg_fetch_{$type}";
            return $drv($rs);
            break;
    }
}

function num_rows($rs) {
    switch (_driver_) {
        default:
        case "mssql":
            return sqlsrv_num_rows($rs);
            break;

        case "mysql":
            return mysql_num_rows($rs);
            break;

        case "postgresql":
            return pg_num_rows($rs);
            break;
    }
}

function anti_injection($data){
  $filter = mysql_real_escape_string(stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
  return $filter;
}

function getKab($id_kab){
    $query = "SELECT id_kabupaten, kabupaten FROM tb_kabupaten WHERE id_kabupaten = $id_kab";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $kabupaten = $r['kabupaten'];
    }
    return $kabupaten;
}

function inKab($id_kab){
    $query = "SELECT * FROM tb_kabupaten WHERE id_kabupaten = $id_kab";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $kabupaten = $r['inisial'];
    }
    return $kabupaten;

}

function inProv($id_prov){
    $query = "SELECT * FROM tb_provinsi WHERE id_provinsi = $id_prov";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $provinsi = $r['inisial'];
    }
    return $provinsi;

}

function getKomoditas($id_komoditas){
    $query = "SELECT * FROM komoditas WHERE id_komoditas= $id_komoditas";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $komoditas = $r['komoditas'];
    }
    return $komoditas;
}

function getBulan($bln) {
    $query = "SELECT nama_bulan FROM bulan WHERE no_bulan = $bln";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $bulan = $r['nama_bulan'];
    }
    return $bulan;
}

function KecTanam($id_kec, $id_komoditas, $bulan, $tahun){
    $query = "SELECT SUM(luas_tambah_tanam) AS luas FROM tambah_tanam 
                WHERE 
                id_kecamatan = $id_kec AND tahun = $tahun
                AND bulan = $bulan AND id_komoditas = $id_komoditas";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $luas_tambah_tanam = $r[luas];
    }
    return $luas_tambah_tanam;
}

function KecLPanen($id_kec, $id_komoditas, $bulan, $tahun){
    $query = "SELECT SUM(luas_panen_habis) AS luas FROM luas_panen 
                WHERE 
                id_kecamatan = $id_kec AND tahun = $tahun
                AND bulan = $bulan AND id_komoditas = $id_komoditas";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $luas_panen = $r[luas];
    }
    return $luas_panen;
}

function KecPanen($id_kec, $id_komoditas, $bulan, $tahun){
    $query = "SELECT SUM(produksi) AS panen FROM panen 
                WHERE 
                id_kecamatan = $id_kec AND tahun = $tahun
                AND bulan = $bulan AND id_komoditas = $id_komoditas";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $panen = $r[panen];
    }
    return $panen;
}

function KecPotensi($id_kec, $id_komoditas, $bulan, $tahun){
    $query = "SELECT SUM(hasil) AS berat FROM tb_prediksi 
                WHERE id_kecamatan = $id_kec AND tahun_panen = $tahun AND bulan_panen = $bulan 
                AND id_komoditas = $id_komoditas";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $potensi = $r[berat];
    }
    return $potensi;

}

function KecCenter($id_kab){
    $query = "SELECT * FROM tb_kecamatan_center WHERE id_kabupaten = $id_kab";
    $result = mysql_query($query);

    while($r = mysql_fetch_array($result)){
        $lat[] = $r[lat];
        $long[] = $r[lng];
        $id_kec[] = $r[id_kecamatan];
    }  
    return array($id_kec, $lat, $long);
}

function KabKomo($id_kab){
    $query = "SELECT komoditas.id_komoditas as id_komo, komoditas.komoditas as komo FROM komoditas
                 INNER JOIN tb_data_dasar 
                 WHERE tb_data_dasar.id_komoditas = komoditas.id_komoditas 
                 AND tb_data_dasar.id_kabupaten = $id_kab";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $id_komoditas[]=$r[id_komo];
        $komoditas[] = $r[komo];
    }
    return array($id_komoditas, $komoditas);
}

function ProvKomo($id_prov){
    $query = "SELECT komoditas.id_komoditas as id_komo, komoditas.komoditas as komo FROM komoditas
                 INNER JOIN tb_data_dasar 
                 WHERE tb_data_dasar.id_komoditas = komoditas.id_komoditas 
                 AND tb_data_dasar.id_provinsi = $id_prov
                 GROUP BY id_komo";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $id_komoditas[]=$r[id_komo];
        $komoditas[] = $r[komo];
    }
    return array($id_komoditas, $komoditas);
}
function getProv($id_prov){
    $query = mysql_query("SELECT * FROM tb_provinsi where id_provinsi=$id_prov");
    while ($r = mysql_fetch_assoc($query)){
        $prov = $r[nama_provinsi];
    }
    return $prov;
}

function KabTanam($id_kab, $id_komoditas, $bulan, $tahun){
    $query = "SELECT SUM(luas_tambah_tanam) AS luas FROM tambah_tanam 
                WHERE 
                id_kab = $id_kab AND tahun = $tahun
                AND bulan = $bulan AND id_komoditas = $id_komoditas";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $luas_tambah_tanam = $r[luas];
    }
    return $luas_tambah_tanam;
}

function KabPanen($id_kab, $id_komoditas, $bulan, $tahun){
    $query = "SELECT SUM(produksi) AS panen FROM panen 
                WHERE 
                id_kab = $id_kab AND tahun = $tahun
                AND bulan = $bulan AND id_komoditas = $id_komoditas";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $panen = $r[panen];
    }
    return $panen;

}

function KabLpanen($id_kab, $id_komoditas, $bulan, $tahun){
    $query = "SELECT SUM(luas_panen_habis) AS luas FROM luas_panen 
                WHERE 
                id_kab = $id_kab AND tahun = $tahun
                AND bulan = $bulan AND id_komoditas = $id_komoditas";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $luas_panen = $r[luas];
    }
    return $luas_panen;
}

function KabPotensi($id_kab, $id_komoditas, $bulan, $tahun){
    $query = "SELECT SUM(hasil) AS berat FROM tb_prediksi 
                WHERE id_kabupaten = $id_kab AND tahun_panen = $tahun AND bulan_panen = $bulan 
                AND id_komoditas = $id_komoditas";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $potensi = $r[berat];
    }
    return $potensi;
}

function ListKab($id_provinsi,$id_komoditas){
    $query = "SELECT tb_kabupaten.id_kabupaten, tb_kabupaten.kabupaten 
                FROM tb_kabupaten INNER JOIN tb_data_dasar 
                WHERE tb_data_dasar.id_kabupaten=tb_kabupaten.id_kabupaten 
                AND tb_data_dasar.id_komoditas='$id_komoditas'
                AND tb_kabupaten.id_provinsi='$id_provinsi'";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $idkab[]=$r[id_kabupaten];
        $kab[]=$r[kabupaten];
    }
    return array($idkab, $kab);
}

function ListBulan(){
    $bulan = mysql_query("SELECT * FROM bulan order BY no_bulan ASC");
    while ($r=mysql_fetch_assoc($bulan)){
        $no_bln[] = $r[no_bulan];
        $bln[] = $r[nama_bulan];
    }
    return array($no_bln, $bln);
}

function ProvTanam($id_kab, $id_komoditas, $bulan, $tahun){
    $query = "SELECT SUM(luas_tambah_tanam) AS luas FROM tambah_tanam 
                WHERE 
                id_prop = $id_kab AND tahun = $tahun
                AND bulan = $bulan AND id_komoditas = $id_komoditas";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $luas_tambah_tanam = $r[luas];
    }
    return $luas_tambah_tanam;
}

function ProvPanen($id_kab, $id_komoditas, $bulan, $tahun){
    $query = "SELECT SUM(produksi) AS panen FROM panen 
                WHERE 
                id_prop = $id_kab AND tahun = $tahun
                AND bulan = $bulan AND id_komoditas = $id_komoditas";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $panen = $r[panen];
    }
    return $panen;

}

function ProvLpanen($id_kab, $id_komoditas, $bulan, $tahun){
    $query = "SELECT SUM(luas_panen_habis) AS luas FROM luas_panen 
                WHERE 
                id_prop = $id_kab AND tahun = $tahun
                AND bulan = $bulan AND id_komoditas = $id_komoditas";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $luas_panen = $r[luas];
    }
    return $luas_panen;
}

function ProvPotensi($id_kab, $id_komoditas, $bulan, $tahun){
    $query = "SELECT SUM(hasil) AS berat FROM tb_prediksi 
                WHERE id_provinsi = $id_kab AND tahun_panen = $tahun AND bulan_panen = $bulan 
                AND id_komoditas = $id_komoditas";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $potensi = $r[berat];
    }
    return $potensi;
}

function ListProv(){
    $query = "SELECT id_provinsi, nama_provinsi FROM tb_provinsi";
    $result = mysql_query($query);
    while ($r = mysql_fetch_assoc($result)){
        $id[] = $r[id_provinsi];
        $prov[] = $r[nama_provinsi];
    }
    return array($id, $prov);
}

function ListKec($id_kab){
	$query = "SELECT id_kecamatan, kecamatan FROM tb_kecamatan WHERE id_kabupaten=$id_kab";
	$result = mysql_query($query);
	while($r = mysql_fetch_assoc($result)){
		$id_kec[] = $r[id_kecamatan];
		$kec[] = $r[kecamatan];
	}
	return array($id_kec, $kec);
}

function getKec($id_kec){
    $query = "SELECT kecamatan FROM tb_kecamatan WHERE id_kecamatan=$id_kec";
    $result = mysql_query($query);
    while($r = mysql_fetch_assoc($result)){
        $kec = $r[kecamatan];
    }
    return $kec;

}
?>